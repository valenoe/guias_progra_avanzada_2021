
public class Velocimetro {
	private double velocidad;
	private double vel_máxima = 120;
	// Velocidad máxima no tiene método set porque nunca cambia
	
	Velocimetro(){
		
	}

	public double getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(double velocidad) {
		
		this.velocidad = velocidad;
	}

	public double getVel_máxima() {
		return vel_máxima;
	}

	
	
	
	

}
