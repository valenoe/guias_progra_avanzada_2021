
public class Rueda {
	private int desgaste;
	
	Rueda(){
		/**El desgaste de los objetos Rueda empieza en 0
		 * Pero con el método setDesgaste() se suma un número aleatorio*/
		this.desgaste = 0;
	}

	public int getDesgaste() {
		return desgaste;
	}

	public void setDesgaste() {
		int desgaste_aleatorio = (int)(Math.random()*10+1);
		this.desgaste = desgaste+desgaste_aleatorio;
	}
	
	

}
