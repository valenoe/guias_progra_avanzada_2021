
public class Motor {
	private String cilindrada;
	Motor(){
		
		// La cilindrada se define al azar
		int azar = (int)(Math.random()*2);
		
		
		if(azar == 0) {
			this.cilindrada = "1.2";
			
		}else { 
			this.cilindrada = "1.6";
		}
		
		
	}
	
	
	public String getCilindrada() {
		return cilindrada;
	}
	public void setCilindrada(String cilindrada) {
		this.cilindrada = cilindrada;
	}
	
	

}
