import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Controlador {
	Controlador() throws IOException {
		Auto a = new Auto();
		
		
		// Se crean e insertan las partes del auto
		Motor m = new Motor();
		a.setMotor(m);
		
		Estanque e = new Estanque();
		a.setTanque(e);
		
		Velocimetro v = new Velocimetro();
		a.setVel(v);
		
		
		// Se insertan las ruedas
		for(int i = 0; i < 4; i++) {
			a.setRuedas(i, new Rueda());
		}
		
		
		System.out.println("****BIENVENIDO A SU AUTO****");
		a.reporte_a();
		
		// Condición del ciclo
		boolean seguir = true;
		while(seguir) {
			/**Se pregunta que quiere hacer con el auto,
			 * encender, apagar o mover
			 * 
			 * Condición:
			 * - Solo se puede mover si está encendido.
			 * - Si después de moverse queda sin combustuble también se cierra el ciclo
			 * */
			InputStreamReader flujo = new InputStreamReader(System.in);
			System.out.println("¿Qué desea hacer con el auto?");
			System.out.println("Encender / Apagar ----> 1");
			System.out.println("   Mover 	  ----> espacio");
		
			BufferedReader br = new BufferedReader(flujo);
			String espacio1 = br.readLine();
			
			if(espacio1.equals("1")) {
				a.setEncendido();
			
			}else if(espacio1.equals(" ")) {
				if(a.isEncendido()) {
					System.out.println("****HA ESCOGIDO AVANZAR****");
					a.movimiento(espacio1);
				
					for(Rueda ru: a.getRuedas()) {
						ru.setDesgaste();	
					}
					a.cambio_ruedas();
					if(a.getTanque().getLitros() <= 0) {
						System.out.println("\n****SE HA QUEDADO SIN COMBUSTIBLE****\n");
						a.getTanque().setLitros(0);
						a.setEncendido();
						
						seguir = false;	
					}
				}else {
					System.out.println("Tiene que encender el auto primero");
				}
			}
			
			a.reporte_a();
			
		}
		System.out.println("\n****YA NO PUEDE USAR EL AUTO****");
		
		
	}
	

}
