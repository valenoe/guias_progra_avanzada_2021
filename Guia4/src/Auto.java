import java.util.ArrayList;

public class Auto {
	private Motor motor;
	private Estanque tanque;
	private Velocimetro vel;
	private ArrayList<Rueda> ruedas;
	private boolean encendido;
	private double distancia_recorrida;
	
	
	// Constructor
	Auto(){
		this.ruedas = new ArrayList<Rueda>();
		this.encendido = false;
		this.distancia_recorrida = 0;
	}

	public Motor getMotor() {
		return motor;
	}

	public void setMotor(Motor motor) {
		this.motor = motor;
	}

	public ArrayList<Rueda> getRuedas() {
		return ruedas;
	}

	public void setRuedas(int i, Rueda rueda_nueva) {
		this.ruedas.add(i, rueda_nueva);
	}

	public boolean isEncendido() {
		return encendido;
	}

	public void setEncendido() {
		/** Método para cambiar el estado del booleano Encendido:
		 * Si está apagado se enciende y se gast el 1 % del combustible.
		 * Si está encendido se apaga.
		 * En ambos se imprime el cambio de Encendido*/
		if(!this.encendido) {
			this.encendido = true;
			System.out.println("****ACABA DE ENCENDERLO****");
			double litros = tanque.getLitros();
			tanque.setLitros(litros-litros/100);
			
		}else {
			this.encendido = false;
			System.out.println("****HA APAGADO Y SALIDO DEL AUTO****");
			
		}
	}

	public Estanque getTanque() {
		return tanque;
	}

	public void setTanque(Estanque tanque) {
		this.tanque = tanque;
	}

	public Velocimetro getVel() {
		return vel;
	}

	public void setVel(Velocimetro vel) {
		this.vel = vel;
	}
	
	

	
	public void  movimiento(String espacio) {
		// variables aleatorias para tiempo y velocidad
		double tiempo_movimiento = (int)(Math.random()*10+1);
		int velocidad = (int)(Math.random()*120+1); 
		
		// Cambio de se segundos a minutos
		double tiempo = tiempo_movimiento/60;
		// Redondeo de la variable tiempo
		tiempo = Math.round(tiempo*10000.0)/10000.0;
		
		
		vel.setVelocidad(velocidad);
		System.out.println("El auto estará en movimiento por: " + tiempo_movimiento + " segundos, o sea, " + tiempo + " minutos"  );
		System.out.println("A " + vel.getVelocidad()+ " km/min");
		imagen_mover(tiempo_movimiento);
		
		/**Se calcula de diatncia:
		 * distancia = velocidad * tiempo
		 * 
		 * Luego Math.round(distancia*10000.0)/10000.0; la deja con 4 decimales 
		 * */
		double distancia = velocidad*tiempo; 
		distancia = Math.round(distancia*10000.0)/10000.0;
		System.out.println("la distancia que recorrió es: " + distancia + " km");
		// Se llama al método que calcula el gasto de combustile
		gasto( velocidad, distancia);
		// Se llama al método que actualiza la distancia 
		setDistancia_recorrida(distancia);
		
	}
	
	public void chequeo_ruedas() {
		// Imprime el estado de las ruedas
		for(int i = 0; i<getRuedas().size(); i++) {
			System.out.println("La rueda: " + (i+1) + " tiene un desgaste de: " + getRuedas().get(i).getDesgaste() + " %");
		}
	}
	

	
	public void gasto(int velocidad, double distancia) {
		/**Método que calcula el gasto de combustible
		 * Dependiendo de la cilindrada el gasto es diferente
		 * 
		 * Cilindrada 1,2  -> 1 litro por 20 km
		 * Cilindrada 1,6  -> 1 litro por 14 km*
		 * 
		 * La variable gastos tambien se redondea a 4 decimales con Math.round(gastos*10000.0)/10000.0;
		 * */
		double gastos;
		if(motor.getCilindrada() == "1.2") {
			gastos = distancia/20;
			
		}else {
			gastos = distancia/14;
		}
		gastos = Math.round(gastos*10000.0)/10000.0;
		
		/**Si el auto no va a velocidad máxima:
		 * Se divide la  velocidad real por la velocidad máxima para obtener la relación
		 * Esto se redondea a 4 decimales.
		 * luego, la relación(porcentaje) se multiplica por el gasto calculado según la cilindrada.
		 * Esto también se redondea a 4 decimales.
		 * El gasto final se le resta a los litros actuales del estanque.
		 * Finalmente, se actualiza el estanque. 
		 * */
		if(velocidad != vel.getVel_máxima()) {
			double porcentaje = vel.getVelocidad()/vel.getVel_máxima();
			porcentaje = Math.round(porcentaje*10000.0)/10000.0;
			gastos = gastos * porcentaje;
			gastos = Math.round(gastos*10000.0)/10000.0;
		}
		
		double l =tanque.getLitros();
		
		tanque.setLitros(l-gastos);
		
	}
	
	
	
	public void reporte_a() {
		System.out.println("\n****CONDICIÓN DEL AUTO****");
		if(this.encendido) {
			System.out.println("- Estado: Encendio");

		}else {
			System.out.println("- Estado: Detenido");
			vel.setVelocidad(0);
		}
		
		System.out.println("- Motor: " + this.motor.getCilindrada());
		System.out.println("- Actualmente el estanque tiene: " + this.tanque.getLitros() + " L");
		
		System.out.println("- Velocidad actual: "+ this.vel.getVelocidad() + " Km/min");
		System.out.println("- Distancia recoorida hasta ahora: " + this.distancia_recorrida + " km");
		
		System.out.println("- Condición de las ruedas:");
		chequeo_ruedas();
		System.out.println("\n");
		
		
	}
	
	public void imagen_mover(double temp) {
		ArrayList<String> imagen = new ArrayList<String>();
		String a, b, c, d, e, f, g, h, i, j; 
		a = "_AUTO_________";
		b = "__AUTO________";
		c = "___AUTO_______";
		d = "____AUTO______";
		e = "_____AUTO_____";
		f = "______AUTO____";
		g = "_______AUTO___";
		h = "________AUTO__";
		i = "_________AUTO_";
		j = "__________AUTO";
		imagen.add(a);
		imagen.add(b);
		imagen.add(c);
		imagen.add(d);
		imagen.add(e);
		imagen.add(f);
		imagen.add(g);
		imagen.add(h);
		imagen.add(i);
		imagen.add(j);
		
		/**Se imprime la imagen del auto moviendose
		 * Se imprime la misma cantidade de imágenes que de segundos que se mueva el auto.
		 * */
		System.out.println("\n****AVANZANDO****");
		for(int m=0; m<temp; m++ ) {
			System.out.println(imagen.get(m));
			
		}
	}

	public double getDistancia_recorrida() {
		return distancia_recorrida;
	}

	public void setDistancia_recorrida(double distancia) {
		this.distancia_recorrida = distancia_recorrida + distancia;
	}
	
	public void cambio_ruedas() {
		/**Método que cambia las reudas:
		 * El contador cont avanza si se debe cambiar una rueda.
		 * Si se debe cambiar alguna rueda (cont>0) el auto se detiene.
		 * 
		 * Para cambiar las ruedas se compara el desgaste con el límite(90).
		 * Si es mayor o igual, la rueda se borra.
		 * Se agrega una rueda nueva en la misma posición que la borrada.*/
		int cont = 0;
		for(int i = 0; i<getRuedas().size(); i++) {
			if(getRuedas().get(i).getDesgaste() >= 90) {
				//setEncendido();
				System.out.println("\n\nLa rueda: " + (i+1) + " tiene un desgaste del "+ ruedas.get(i).getDesgaste()+" %, se debe cambiar");
				getRuedas().remove(i);
				//ruedas.add(i, new Rueda());
				setRuedas(i, new Rueda());
				cont++;
			}
		}
		if(cont>0) {
			System.out.println("\nEl auto debe detenerse para cambiar la(s) rueda(s)");
			setEncendido();
			System.out.println("\nYa cambió la(s) rueda(s)");
			//setEncendido();
		}
	}
	

}
