
public class Tarjeta {
	// Tarjeta para la biblioteca
	private int saldo;
	private Persona p;
	
	//Empieza con un saldo de 10000
	Tarjeta(){
		this.saldo = 10000;
	}
	
	public int getSaldo() {
		
		return saldo;
	}
	
	public void setSaldo(int multa) {
		// el saldo se modifica cuando recibe una multa
		this.saldo = saldo - multa;
	}
	public Persona getP() {
		return p;
	}
	public void setP(Persona p) {
		this.p = p;
	}
	
	public void muestra_saldo() {
		// muestra si debe o no a la biblioteca
		if(saldo < 0) {
			System.out.println(p.getNombre() + " le debe a la biblioteca $" + (-1*saldo));
		}else {
			System.out.println("Usted tiene " + saldo + " como saldo en su tarjea de biblioteca\n");
		}
	}
	

}
