
public class Documento {
	// Atributos generales
	private int codigo;
	private int anio_publicacion;
	private String titulo;
	private boolean prestado;
	
	public Documento(String nombre, int codigo, int anio) {
		this.titulo = nombre;
		this.codigo = codigo;
		this.anio_publicacion = anio;
		this.prestado = false;
	}
	
	// Los atributos de estos métodos solo pueden mostrarse o llamarse, no cambiarse
	public int getCodigo() {
		return codigo;
	}
	public int getAnio_publicacion() {
		return anio_publicacion;
	}

	public String getTitulo() {
		return titulo;
	}
	
	// prestado si puede cambiar
	public boolean isPrestado() {
		return prestado;
	}

	public void setPrestado(boolean prestado) {
		this.prestado = prestado;
	}
	
	// Los 3 métodos siguientes muestran detalles de los documentos
	public void muestra_detalle() {
		if(prestado) {
			System.out.println(titulo + ", " + anio_publicacion + ", ISBN: " + codigo + " - Prestado");
			
		}else {
			System.out.println(titulo + ", " + anio_publicacion + ", ISBN: " + codigo + " - No prestado");
			
		}
	}
	public void muestra_anio() {
		System.out.println("El anio de publicacion es: " + this.anio_publicacion);
	}
	public void muestra_codigo() {
		System.out.println("El codigo del libro es: " + this.codigo);
	}
}
