
public class Main {

	public static void main(String[] args) {
		
		Revista r1 = new Revista("Hola mundo - Benz", 12035488, 2020, 2, "Cientifica");
		Libro l1 = new Libro("Hello world - Roberto Campos", 12036558, 2021);
		
		
		Revista r2 = new Revista("Cocinando Peru - M.K.", 32447852, 2019, 5, "Gastronomica");
		Libro l2 = new Libro("Introduccion a la pogramacion orientada a objetos con java - Rafael Llobet", 12033201, 2020);
		
		Revista r3 = new Revista("Lore Olympus - Usedbandai ", 54782213, 2019, 1, "Comic");
		Libro l3 = new Libro("Miau dijo el perro un dia - M.R.", 19215640, 2014);
		
		// Añade los libros y revistas recién creados
		Biblioteca biblio = new Biblioteca();
		biblio.setLibros(l1);
		biblio.setLibros(l2);
		biblio.setLibros(l3);
		biblio.setRevistas(r1);
		biblio.setRevistas(r2);
		biblio.setRevistas(r3);
		
		// Muestra los objetos de cada una de las listas
		System.out.printf("LIBROS\n");
		biblio.muestra_Libros(biblio.getLibros());
		System.out.printf("\n");
		System.out.printf("REVISTAS\n");
		biblio.muestra_Revistas(biblio.getRevistas());
		
		
		System.out.printf("\n*****************************************************************\n");
		
		Tarjeta t =new Tarjeta();
		Persona p = new Persona("Carlos Gomez");
		p.setT(t);
		t.setP(p);
		
		
		System.out.printf("Bienvenido(a) " + p.getNombre() +" a la biblioteca\n");
		
		int escoge_l_o_r = (int)(Math.random()*2);
		if(escoge_l_o_r == 0) {
			//libro
			int aleatorio = (int)(Math.random()*biblio.getLibros().size());
			System.out.println("Ha escogido el libro n° "+ (aleatorio + 1) +" de la lista");
			biblio.getLibros().get(aleatorio).prestar();
			biblio.getLibros().get(aleatorio).muestra_detalle();
			biblio.getLibros().get(aleatorio).prestado();
			int dias_extra = (int)(Math.random()*10+1);
			
			System.out.println();
			biblio.getLibros().get(aleatorio).setDia_actual(dias_extra);
			
			int multa = biblio.getLibros().get(aleatorio).devolver();
			// Aquí si se puede aplicar una multa ya que el documento es un libro
			t.setSaldo(multa);
			t.muestra_saldo();
			
		}else {
			// Revista
			int aleatorio = (int)(Math.random()*biblio.getLibros().size());
			System.out.println("Ha escogido la revista n° "+ (aleatorio + 1) +" de la lista");
			int dias_extra = (int)(Math.random()*20+1);
			biblio.getRevistas().get(aleatorio).prestar();
			biblio.getRevistas().get(aleatorio).muestra_detalle();
			biblio.getRevistas().get(aleatorio).prestado();
			System.out.println();
			biblio.getRevistas().get(aleatorio).setDia_actual(dias_extra);
			biblio.getRevistas().get(aleatorio).devolver();
		}
		
		
	}

}
