import java.time.*;
public class Libro extends Documento implements controlador{

	// Atributos útiles para prestar el libro
	private LocalDate dia_de_prestamo;
	private LocalDate dia_actual;
	
	Libro(String nombre, int codigo, int anio){
		super(nombre, codigo, anio);
		
	}
	
	
	// De aquí en adelante son métodos útiles para prestarse
	public void prestar(){
		/**
		 * Si el libro no está prestado:
		 * se cambia el valor del booleano "prestado"
		 * y se define la fecha de prestamo como el día de hoy*/
		if(!super.isPrestado()) {
			setPrestado(true);
			this.dia_de_prestamo = LocalDate.now();
			
		}else {
			System.out.println("Este libro ya esta prestado");
		}
		
	}
	public int devolver() {
		int multa =0;
		/** Se crea y define la variable multa en 0,
		 * esta cambiará si la persona se atrasa en devolver el libro
		 * 
		 * En una linea se imprime el día que se realizó el prestamo (dia_de_prestamo)
		 * y el día que se está devolviendo el libro(dia_actual)
		 * 
		 * Se compara dia_actual con dia_de_prestamo + 6,
		 * o sea, se compara el día actual con el día que debió devolver el libro
		 * 
		 * el plazo de devolución es 5 días, se coloca 6 al comparar 
		 * para que el día 5 esté dentro del plazo
		 * 
		 * Si se devolvió fuera del plazo se multa a la persona,
		 * 1290 por día de atraso
		 * Para saber cuantos días se atrasó se obtiene el n° del día de dia_actual(dia) 
		 * y el n del día de dia_de_prestamo(dia_original), estos se restan y también se resta 5
		 * ya que es el plazo limite
		 * 		 dia = dia-dia_original - 5;
		 * los $1290 se multiplican por el nuevo valor de la variable dia y este retorna al final del método
		 * también "prestado" vuelve a ser false
		 * */
		System.out.println("Dia del prestamo:" +this.dia_de_prestamo + ", dia actual: "+ this.dia_actual);
		if(this.dia_actual.isBefore(this.dia_de_prestamo.plusDays(6))) {
			System.out.println("Usted ha devuelto el libro dentro del plazo de tiempo");	
				
		}else {
			System.out.println("Usted ha devuelto el libro fuera de plazo");
			System.out.println("Se le aplicara una multa de 1290 por dia de retraso");
			
			int dia  = this.dia_actual.getDayOfMonth();
			int dia_original = this.dia_de_prestamo.getDayOfMonth();
			dia = dia-dia_original - 5;
			multa = 1290*dia;
			System.out.println("Usted se atraso " + dia + " dias, se le aplicara una multa de: " + multa);
		}
		this.setPrestado(false);
		return multa;
	}
	public void prestado() {
		/**Este metodo muestra si el libro está prestado o no
		 * Cual es el plazo de devolucion 
		 * y que pasa si no lo devuelve dentro de ese plazo
		 */
		if(super.isPrestado()) {
			System.out.println("Usted tiene 5 dias para devolver el libro o se le aplicara una multa");
			System.out.println("Seran $1290 por cada dia de retraso");
		}else {
			System.out.println("El libro no se ha prestado");
		}
		
	}
	
	
	public LocalDate getDia_de_prestamo() {
		return dia_de_prestamo;
	}


	public LocalDate getDia_actual() {
		return dia_actual;
	}

	public void setDia_actual(int aleatorio) {
		/**
		 * En main se define un n° aleatorio, 
		 * este se suma al día que se prestó el libro para
		 * obtener el día actual, el día que la persona va a devolver el libro
		 * */
		this.dia_actual = this.dia_de_prestamo.plusDays(aleatorio);
	}
	
	

	
	
	

}
