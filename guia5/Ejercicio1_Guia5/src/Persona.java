
public class Persona {
	private Tarjeta t;
	private String nombre;
	
	// Persona solo existe para tener la tarjeta y recibir el libro
	public Persona(String nombre) {
		this.nombre = nombre;
	}

	public Tarjeta getT() {
		return t;
	}

	public void setT(Tarjeta t) {
		this.t = t;
	}

	public String getNombre() {
		return nombre;
	}

}
