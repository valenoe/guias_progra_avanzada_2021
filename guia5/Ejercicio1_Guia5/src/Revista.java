import java.time.LocalDate;


public class Revista extends Documento implements controlador{
	
	//Atributos específicos
	private int n_edicion;
	private String genero;
	private boolean caducida;
	
	// Atributos útiles para prestar la revista
	private LocalDate dia_de_prestamo;
	private LocalDate dia_actual;
	private int dias_devolucion;
	
	
	Revista(String nombre, int codigo, int anio, int edicion, String genero){
		super(nombre, codigo, anio);
		this.n_edicion = edicion;
		this.genero = genero;
		
		/** Se define si la revista está caduca por un n° aleatorio
		* 0 --> está caduca
		* 1 --> no está caduca
		* */
		int caducida = (int)(Math.random()*2);
		if(caducida == 0) {
			this.caducida = true;
		}else {
			this.caducida = false;
		}
		
	}
	
	public void muestra_edicion() {
		System.out.println("El numero de edición es: " + this.n_edicion);
	}
	
	// Los atributos de estos método solo pueden mostrarse o llamarse, no cambiarse
	public int getN_edicion() {
		return n_edicion;
	}
	
	
	public String getGenero() {
		return genero;
	}
	public boolean getCaducida() {
		return caducida;
	}
	
	// De aquí en adelante son métodos útiles para prestarse
	public void setDias_devolucuon() {
		// Días de prestamo
		//1 --> Comic, 2 --> Científica y Deportiva, 3 --> Gastronónimca
		/**
		 * Si la revista no está caduca los comics tienen 24 horas para devolverse,
		 * si no, no tienen limite de devolucion, para esto es necesario un numero muy grande
		 * 
		 * Se coloca un dia de más para que el día límite tambien cunete dentro del plazo
		 * */
		if (this.genero.equals("Comic")) {
			if(!caducida) {
				this.dias_devolucion = 2;
			}else {
				this.dias_devolucion = 1000000;
			}
		/**
		 * Para los demás generos la revista no caduca
		 * */
		}else if(this.genero.equals("Cientifica")|| this.genero.equals("Deportiva")) {
			this.dias_devolucion = 3;
		}else if(this.genero.equals("Gastronomica")){
			this.dias_devolucion = 4;
		}
		
	}
	
	public void prestar(){
		/**
		 * Si la revista no está prestada:
		 * se cambia el valor del booleano "prestado"
		 * y se define la fecha de prestamo como el día de hoy*/
		if(!super.isPrestado()) {
			setPrestado(true);
			this.dia_de_prestamo = LocalDate.now();
			
		}else {
			System.out.println("Esta revista ya esta prestada");
		}
		
	}
	
	public int devolver() {
		int multa = 0;
		/** La multa siempre va a ser 0 porque las revistas no descuentan dinero por atraso,
		* pero como la interfaz está definida como int, el método debe devolver algo
		* */
		
		/** Se imprime el día de prestamo y el día que se devuelve la revista,
		 * si está caduca simpre se devuelve dentro del plazo, 
		 * si no, depede de cada genero:
		 * 		se compara dia_actual(día que se devuelve la revista) con dia_de_prestamo + dias_de_devolucion,
		 * 		o sea, se compara el día actual con el día que debió devolver la revista
		 * 
		 * Al final, el booleano "prestamo" vuelve a cambiar, esta vez a false
		 * */
		 
		System.out.println("Dia del prestamo:" +this.dia_de_prestamo + ", dia actual: "+ this.dia_actual);
		
		if(this.caducida && this.genero.equals("Comic")) {
			System.out.println("Usted ha devuelto la revista dentro del plazo de tiempo");	
			System.out.println("Ya que al estar caduca el plazo es indefinido");	
			
			
		}else {
			if(this.dia_actual.isBefore(this.dia_de_prestamo.plusDays(this.dias_devolucion))) {
				System.out.println("Usted ha devuelto la revista dentro del plazo de tiempo");	
				
			}else {
				System.out.println("Usted ha devuelto la revista fuera de plazo");
				System.out.println("No se le aplicara una multa por dia de retraso");
			
			}
		}
		this.setPrestado(false);
		return multa;
	}
	public void prestado() {
		/**Este metodo muestra si la revista está prestada o no
		 * También llama al método setDias_devolucuon() para asignarle los días 
		 * que debe devolverse cada revista según su genero
		 *  */
		setDias_devolucuon();
		if(super.isPrestado()) {
			
			if(this.dias_devolucion > 3) {
				System.out.println("Usted tiene no tiene plazo para devolver la revista ya que es un " + this.genero + " caduco");
				
			}else {
				System.out.println("Usted tiene " + this.dias_devolucion + " dias para devolver la revista ya que es " + this.genero );
				
			}
			
		}else {
			System.out.println("La revista no esta prestada" );
		}
		
	}
	
	public LocalDate getDia_de_prestamo() {
		return dia_de_prestamo;
	}
	
	public int getDias_devolucion() {
		return dias_devolucion;
	}
	public LocalDate getDia_actual() {
		return dia_actual;
	}
	
	public void setDia_actual(int aleatorio) {
		/**
		 * En main se define un n° aleatorio, 
		 * este se suma al día que se prestó la revista para
		 * obtener el día actual, el día que la persona va a devolver la revista
		 * */
		this.dia_actual = this.dia_de_prestamo.plusDays(aleatorio);
	}
	

	
	

}
