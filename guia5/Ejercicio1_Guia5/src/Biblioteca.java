import java.util.ArrayList;

public class Biblioteca {
	private ArrayList<Libro> libros;
	private ArrayList<Revista> revistas;
	
	// constructor de Biblioteca
	public Biblioteca() {
		libros = new ArrayList<Libro>();
		revistas = new ArrayList<Revista>();
	}
	

	
	public ArrayList<Libro> getLibros() {
		return libros;
	}
	public void setLibros(Libro l) {
		this.libros.add(l);
	}
	public ArrayList<Revista> getRevistas() {
		return revistas;
	}
	public void setRevistas(Revista r) {
		this.revistas.add(r);
	}
	
	public void muestra_Libros(ArrayList<Libro> libros) {
		//Método que muestra los libros de la biblioteca
		for(Libro l: libros) {
			l.muestra_detalle();
			l.muestra_anio();
			l.muestra_codigo();
			
			System.out.printf("\n");
		}
	}
	
	public void muestra_Revistas(ArrayList<Revista> rr) {
		//Método que muestra las revistas de la biblioteca
		for(Revista r: rr) {
			r.muestra_detalle();
			r.muestra_anio();
			r.muestra_codigo();
			r.muestra_edicion();
			System.out.printf("\n");
		}
	}
}
