
public class Mainnn {

	public static void main(String[] args) {
		
		Controlador c = new Controlador();
		//Se crean las personas de cada tipo
		Estudiante e = new Estudiante("Jose Manuel", "Contreras", 2021242001, "Programacion avanzada");
		Profesor p = new Profesor("Karla", "Valenzuela", 2018333306, 2018, 3306, "Bioinformática");
		Administrativo a = new Administrativo("Juan", "Gutierrez", 2015122231, 2015, 2231, "Biblioteca");
		Estudiante e1 = new Estudiante("Verónica ", "valenzuela", 2019430201, "Probabilidad y Estadística");
		Estudiante e2 = new Estudiante("Ignacio", "Benavides", 2021430033, "COE I");
		
		// Se agregan a los ArrayList correspondientes
		c.setAdministrativos(a);
		c.setEstudiantes(e);
		c.setEstudiantes(e1);
		c.setEstudiantes(e2);
		c.setProfesores(p);
		
		// Se imprimen los ArrayList
		c.print(c.getEstudiantes(),c.getProfesores(), c.getAdministrativos());
	}

}
