
public class Persona {
	// Atributos generales
	private String nombre;
	private String apellido;
	private int n_identificacion;
	
	public Persona(String nombre, String apellido, int numero) {
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setN_identificacion(numero);
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getN_identificacion() {
		return n_identificacion;
	}

	public void setN_identificacion(int n_identificacion) {
		this.n_identificacion = n_identificacion;
	}
	
	public void mostrar() {
		// muestra los atributos generales
		System.out.println(nombre + " " + apellido + ", Número de identificación: " + n_identificacion );
	}
	
	
}
