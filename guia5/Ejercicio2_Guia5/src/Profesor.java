
public class Profesor extends Empleado{
	// Atributo específico
	private String departamento;
	
	public Profesor(String nombre, String apellido, int numero, int anio, int anexo, String depart) {
		// con super puede rellenar los atributos que hereda, o sea a empleado
		super(nombre, apellido, numero, anio, anexo);
		this.departamento = depart;
	}
	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	
	public void mostrar() {
		// Muestra el atributo específico y con super los que hereda
		super.mostrar();
		System.out.println("Tipo de empleado: Profesor, departamento: " + departamento);
	}
	
	

}
