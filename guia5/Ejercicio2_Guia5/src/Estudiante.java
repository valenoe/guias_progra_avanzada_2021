
public class Estudiante extends Persona{
	// Atributo específico
	private String curso;
	
	public Estudiante(String nombre, String apellido, int numero, String curso) {
		// con super puede rellenar los atributos que hereda, o sea, a persona
		super(nombre, apellido, numero);
		this.curso = curso;
		
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}
	
	public void mostrar() {
		// Muestra el atributo específico y con super los que hereda
		super.mostrar();
		System.out.println("Tipo de persona: Estudiante, curso: " + curso);
	}
	
}
