
public class Administrativo extends Empleado{
	// Atributo específico
	private String seccion;
	
	public Administrativo(String nombre, String apellido, int numero, int anio, int anexo, String seccion) {
		// con super puede rellenar los atributos que hereda, o sea a empleado
		super(nombre, apellido, numero, anio, anexo);
		this.seccion = seccion;
		
	}

	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	
	public void mostrar() {
		// Muestra el atributo específico y con super los que hereda
		super.mostrar();
		System.out.println("Tipo de empleado: Administratuvo, sección: " + seccion);
	}

}
