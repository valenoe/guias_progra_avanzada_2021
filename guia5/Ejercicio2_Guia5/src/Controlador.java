import java.util.ArrayList;

public class Controlador {
	
		// ArrayList que contendran a cada tipo de persona
		private ArrayList<Estudiante> estudiantes;
		private ArrayList<Profesor> profesores;
		private ArrayList<Administrativo> administrativos;
		
	Controlador(){
		this.estudiantes = new ArrayList<Estudiante>();
		this.profesores = new ArrayList<Profesor>();
		this.administrativos = new ArrayList<Administrativo>();
		
		
		
	}
	public ArrayList<Estudiante> getEstudiantes() {
		return estudiantes;
	}
	public void setEstudiantes(Estudiante newestudiante) {
		this.estudiantes.add(newestudiante);
	}
	public ArrayList<Profesor> getProfesores() {
		return profesores;
	}
	public void setProfesores(Profesor newprofesore) {
		this.profesores.add(newprofesore);
	}
	public ArrayList<Administrativo> getAdministrativos() {
		return administrativos;
	}
	public void setAdministrativos(Administrativo newadministrativo) {
		this.administrativos.add(newadministrativo);
	}
	
	public void print(ArrayList<Estudiante> estudiantes, ArrayList<Profesor> profesores, ArrayList<Administrativo> administrativos){
		// Imprime el contenido de los ArrayList
		System.out.println("\nESTUDIANTES **********");
		for(Estudiante e: estudiantes) {
			e.mostrar();
			System.out.println("");
		}
		System.out.println("\nPROFES **********");
		for(Profesor p: profesores) {
			p.mostrar();
			System.out.println("");
		}
		System.out.println("\nADMINISTRATIVOS **********");
		for(Administrativo a: administrativos) {
			a.mostrar();
			System.out.println("");
		}
	
		
	}

}
