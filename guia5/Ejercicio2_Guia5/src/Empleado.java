
public class Empleado extends Persona{
	// Atributos específicos
	private int anio_incorporacion;
	private int n_anexo;
	
	public Empleado(String nombre, String apellido, int numero, int anio, int anexo) {
		// con super puede rellenar los atributos que hereda, o sea, a persona
		super(nombre, apellido, numero);
		this.anio_incorporacion = anio;
		this.n_anexo = anexo;
	}

	public int getAnio_incorporacion() {
		return anio_incorporacion;
	}

	public void setAnio_incorporacion(int anio_incorporacion) {
		this.anio_incorporacion = anio_incorporacion;
	}

	public int getN_anexo() {
		return n_anexo;
	}

	public void setN_anexo(int n_anexo) {
		this.n_anexo = n_anexo;
	}
	
	public void mostrar() {
		// Muestra los atributos específicos y con super los que hereda
		super.mostrar();
		System.out.println("Tipo de persona: Empleado, año incorporacón: " + anio_incorporacion + ", número de anexo: " + n_anexo );
	}
	
}
