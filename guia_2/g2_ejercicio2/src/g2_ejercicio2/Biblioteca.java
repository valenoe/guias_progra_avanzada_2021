package g2_ejercicio2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Biblioteca {
	private Libro l;
	private ArrayList<Libro> biblioteca = new ArrayList<Libro>();
	BufferedReader reader = new BufferedReader( new InputStreamReader(System.in));
	
	// con esto puedo crear un libro
	public Libro getLibro() {
		return l;
	}

	public void setLibro(Libro libro) {
		this.l = libro;
	}

	// con esto puedo agregar libros a la biblioteca
	public void agregarLibro(Libro libro) {
		biblioteca.add(libro);
			
	}
	
	public ArrayList<Libro> getBiblioteca() {
		return biblioteca;
	}
	
	// Muestra los datos de los libros dentro del ArrayList biblioteca
	public void printBiblioteca (ArrayList<Libro> biblioteca) {
		for(Libro l: biblioteca ){
			int resto = l.getN_ejemplares_totales() - l.getN_ejemplares_prestados();
			System.out.println("\nNúmero del libro: " + (biblioteca.indexOf(l) + 1));
			System.out.println("Titulo: "+l.getTitulo() + "\nAutor: "+l.getAutor() + "\nEjemplares totales: "+ 
			l.getN_ejemplares_totales()+ "\nEjemplares disponibles: " + resto + "\n");
		}
	
	}
	
	public void borrarLibro() throws IOException {
		/**
		 * Se pide el indice del libro que desea borrar.
		 * Se comprueba que este esté dentro del ArrayList biblioteca
		 * y se muestra por última vez la información del libro
		 * */
		System.out.println("\n\nIndique el número del libro que quiere borrar");
		String n = reader.readLine ();
		int n_libro = Integer.parseInt(n) - 1;
		if(n_libro < biblioteca.size()) {
		System.out.println("Usted quiere borrar: "+ biblioteca.get(n_libro).getTitulo() +
				"\nDel autor/a: " +biblioteca.get(n_libro).getAutor());
		// borra el libro
		biblioteca.remove(n_libro);
		}else {
			System.out.println("Este indice no existe en la biblioteca");
			System.out.println("La biblioteca no cambiará");

		}
		
		System.out.println("\n");
		
	}
	
	public void agregarLibro() throws IOException {
		/**
		 * Se crea un nuevo libro vació 
		 * sus datos se completaran por terminal
		 * estos se agregaran al libro 
		 * y finalmente este al ArrayList biblioteca*/
		Libro l9 = new Libro();
		System.out.println("Rellene los datos del libro nuevo");
		
		System.out.println("Ingrese el titulo");
		String titulo = reader.readLine ();
		System.out.println("Ingrese el nombre del autor/a");
		String nombre_a = reader.readLine ();
		System.out.println("Ingrese la cantidad de ejemplares que va a donar a la biblioteca");
		String n_ejemplares = reader.readLine ();
		int n_ejemplares_t = Integer.parseInt(n_ejemplares);
		
		l9.setTitulo(titulo);
		l9.setAutor(nombre_a);
		l9.setN_ejemplares_totales(n_ejemplares_t);
		
		biblioteca.add(l9);
		
		System.out.println("Despues de haber agreado un libro:");
		
		
	}
	
	public void modificarLibro() throws IOException {
		//Libro l9 = new Libro();
		/**
		 * se pide el indice del libro que desea borrar.
		 * Se convierte en entero. 
		 * Se evalúa si es que el indice está dentro de la biblioteca,
		 * si es así, se piden los datos nuevos y se cambian en el libro correspondiente
		 * */
		System.out.println("Indique el indice del libro que desea cambiar");
		String indice_s = reader.readLine();
		int indice = Integer.parseInt(indice_s) - 1;
		
		if(indice < biblioteca.size()) {
			System.out.println("Rellene los datos correctos del libro");
			
			System.out.println("Ingrese el titulo");
			String titulo = reader.readLine ();
			System.out.println("Ingrese el nombre del autor/a");
			String nombre_a = reader.readLine ();
			System.out.println("Ingrese la cantidad de ejemplares que va a donar a la biblioteca");
			String n_ejemplares1 = reader.readLine ();
			int n_ejemplares_t1 = Integer.parseInt(n_ejemplares1);
			
			biblioteca.get(indice).setTitulo(titulo);
			biblioteca.get(indice).setAutor(nombre_a);
			biblioteca.get(indice).setN_ejemplares_totales(n_ejemplares_t1);
			System.out.println("Despues de haber modificado un libro:");
		} else {
			System.out.println(" este indice no existe en la biblioteca");
			System.out.println("La biblioteca no cambiará");

		}
		
		
		
		
	}
	
	 public void prestamo() throws IOException {
		 	/**
		 	 * Se pide el indice del libro que quiere
		 	 * y se evalúa si es que está dento de la biblioteca*/
	        System.out.println("Indique el indice del libro que desea recibir");
			String indice_s = reader.readLine();
			int indice = Integer.parseInt(indice_s) - 1;
			
			if(indice < biblioteca.size()) {
				/**
				 * boolean prestado va a ser true siempre y cuando hayan suficientes libros para prestar*/
				boolean prestado = true;
				/**
				 * biblioteca.get(indice).getN_ejemplares_prestados() representa a los libros prestados antes de este método, 
				 * esto aumenta si se logra prestar un libro
				 * biblioteca.get(indice).getN_ejemplares_totales() representa a la cantidad total de libros de cierto indice*/
				if (biblioteca.get(indice).getN_ejemplares_prestados()< biblioteca.get(indice).getN_ejemplares_totales()) {
	            //prestados ++;
					biblioteca.get(indice).setN_ejemplares_prestados(biblioteca.get(indice).getN_ejemplares_prestados()+1);
				}else {
					prestado = false;
					}
				if(prestado){
					System.out.println("Se ha prestado el libro " + biblioteca.get(indice).getTitulo());
				} else {
					System.out.println("No quedan ejemplares del libro " + biblioteca.get(indice).getTitulo()+ " para prestar");         
				}
			} else {
					
					System.out.println(" este indice no existe en la biblioteca");
					System.out.println("La biblioteca no cambiará");

				}
			
		
	       
	    }
	 
	 public void devolucion() throws IOException {
		 	// para devolver libros
	        System.out.println("Indique el indice del libro que desea devolver");
			String indice_s = reader.readLine();
			int indice = Integer.parseInt(indice_s) - 1;
			
			if(indice < biblioteca.size()) {
				/**
				 * biblioteca.get(indice).getN_ejemplares_prestados() representa a los libros prestados antes de este método, 
				 * esto disminuye si se devuelve un libro*/
				biblioteca.get(indice).setN_ejemplares_prestados(biblioteca.get(indice).getN_ejemplares_prestados()-1);
				System.out.println("Se ha devuelto el libro " + biblioteca.get(indice).getTitulo());
			} else {
					
					System.out.println(" este indice no existe en la biblioteca");
					System.out.println("La biblioteca no cambiará");

				}
			
		
	       
	    }
	
	
}
