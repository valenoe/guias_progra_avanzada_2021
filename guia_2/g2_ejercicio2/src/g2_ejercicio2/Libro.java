package g2_ejercicio2;



public class Libro {
	private String titulo;
	private String autor;
	private int n_ejemplares_totales;
	private int n_ejemplares_prestados;
	
	// Constructor por defecto
	public Libro() {
		
	}
	
	// constructor con parámetros
	public Libro(String titulo, String autor,int totales, int prestados){
		this.titulo = titulo;
		this.autor = autor;
		this.n_ejemplares_totales = totales;
		this.n_ejemplares_prestados = prestados;
	}
	
	
	
	
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public int getN_ejemplares_totales() {
		return n_ejemplares_totales;
	}
	public void setN_ejemplares_totales(int n_ejemplares_totales) {
		this.n_ejemplares_totales = n_ejemplares_totales;
	}
	public int getN_ejemplares_prestados() {
		return n_ejemplares_prestados;
	}
	public void setN_ejemplares_prestados(int n_ejemplares_prestados) {
		this.n_ejemplares_prestados = n_ejemplares_prestados;
	}
	
	
	

}
