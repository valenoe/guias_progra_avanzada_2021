package g2_ejercicio2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Main_e2 {

	public static void main(String[] args) throws IOException {
		
		Biblioteca b = new Biblioteca ();
		BufferedReader reader = new BufferedReader( new InputStreamReader(System.in));
		ArrayList<Libro> biblio = new ArrayList<Libro>();
		
		System.out.println("		Bienvenido a la biblioteca		");
		
		/**
		 * Biblioteca comienza con 8 libros*/
		System.out.println("Estos libros tiene la biblioteca actualmente\n");
		Libro l1, l2, l3, l4, l5, l6, l7, l8;
		l1 = new Libro("Del amor y otros demonios", "Gabriel García Marquez", 5, 2);
		b.agregarLibro(l1);
		l2 = new Libro("Harry Potter y la pridra filosofal", "J. K. Rowling", 10, 8 );
		b.agregarLibro(l2);
		l3 = new Libro("1984", "George Orwell", 2, 2);
		b.agregarLibro(l3);
		l4 = new Libro("Anne Of Green Gables", "L. M. Montgomery", 10, 7);
		b.agregarLibro(l4);
		l5 = new Libro("Papelucho en la clinica", "Marcela Paz", 5, 4);
		b.agregarLibro(l5);
		l6 = new Libro("Muchas vidas muchos maestros", "Brian Weiss", 1, 0);
		b.agregarLibro(l6);
		l7 = new Libro("EL libro salvaje", "Juan Villoro", 3, 1);
		b.agregarLibro(l7);
		l8 = new Libro("Juego de tronos", "George R. R. Martin", 6, 6);
		b.agregarLibro(l8);
		
		b.printBiblioteca( b.getBiblioteca());
		// simepre se muestran los libros al princpio
		biblio = b.getBiblioteca();
		
		// menu de acción
		System.out.println("¿Que desea hacer?");
		System.out.println("1	Ver los libros y su información");
		System.out.println("2	Borrar un libro");
		System.out.println("3	Agregar un libro");
		System.out.println("4	Modificar un libro");
		System.out.println("5	Tomar prestado un libro");
		System.out.println("6	Devolver un libro");
		String respuesta = reader.readLine();
		// se convierte la respuesta a entero
		int r_respuesta = Integer.parseInt(respuesta);
		switch(r_respuesta) {
		case 1: b.printBiblioteca(biblio); break;
		case 2: b.borrarLibro();
				System.out.println("Así queda la nueva biblioteca");
				b.printBiblioteca(biblio); break;
		case 3: b.agregarLibro();
				System.out.println("Así queda la nueva biblioteca");
				b.printBiblioteca(biblio); break;
		case 4: b.printBiblioteca(biblio); 
				b.modificarLibro();
				b.printBiblioteca(biblio); break;
		case 5: b.prestamo(); break;
		case 6: b.devolucion(); break;
		default: break;
		}
		
		System.out.println("Gracias por visitar la biblioteca");
	
		
		
		  /*AQUÍ HAY UN ERROR 
		 * NO SE CUAL ES
		 * TAMBIEN SE VA A LA BIBLIOTECA*//*
		System.out.println("Para buscar un libro ingrese el título por favor");
		String titulo_buscado = reader.readLine ();
		String t = titulo_buscado.toUpperCase();
		System.out.println(t);
		
		if(....)) {
			System.out.println("Encontramos el libro:\n"+ 
					"Titulo: "+ l.getTitulo() + "\nDel autor/a: " +l.getAutor()+
					"Ejemplares totales: " + l.getN_ejemplares_totales());
		} else {
			System.out.println("No ta");
		/*for (Libro l: biblio) {
			System.out.println(l.getTitulo().toUpperCase());
			String l1 = l.getTitulo().toUpperCase();
			if(t == l.getTitulo().toUpperCase()) {
				System.out.println("Encontramos el libro:\n"+ 
						"Titulo: "+ l.getTitulo() + "\nDel autor/a: " +l.getAutor()+
						"Ejemplares totales: " + l.getN_ejemplares_totales());
			} else {
				System.out.println("No ta");
			}
		}*//*
		String t3 = "JUEGO DE TROnos";
		String t2 = t3.toUpperCase();
		String t1 = biblio.get(7).getTitulo().toUpperCase();
		System.out.println(t2);
		System.out.println(biblio.get(7).getTitulo().toUpperCase());
		//System.out.println(biblio.indexOf(biblio.get(7).getTitulo()));
		if(t2 == t1) {
			System.out.println(t2);
			System.out.println(biblio.get(7).getTitulo().toUpperCase());
		}else {
			System.out.println("no");
		}
		*/
		
		
	}
	
	

}
