package g2_ejercico3;

public class Perro {
	private String nombre;
	private String pelaje;
	private String raza;
	private String sexo;
	private double edad;
	private double peso;
	
	// el nombre se lo dará después su nuevo dueño
	public Perro( double p_edad, String p_pelaje,String p_raza, String p_sexo, double p_peso) {
		this.edad = p_edad;
		this.pelaje = p_pelaje;
		this.raza = p_raza;
		this.sexo = p_sexo;
		this.peso = p_peso;
		
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPelaje() {
		return pelaje;
	}
	public void setPelaje(String pelaje) {
		this.pelaje = pelaje;
	}
	public String getRaza() {
		return raza;
	}
	public void setRaza(String raza) {
		this.raza = raza;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public double getEdad() {
		return edad;
	}
	public void setEdad(double edad) {
		this.edad = edad;
	}
	
	public void ladrar() {
		System.out.println("Guau guau");
		
	}
	
	public void jugar(boolean hembra) {
		/**
		 * Se encanrga de imprimir todo lo que hace un perro
		 * cuando su amo le tira una pelota
		 * */
		System.out.println("\nTu:	-¡Vamos " +getNombre() + "! ¡Ve por la pelota!");
		/**
		 * boolean hembra = true cuando el perro es hembra 
		 * o flase cuando es macho
		 * */
		if(hembra) { 
			System.out.println("	-Eso mi niña ¡Traela! ");
			
		} else {
			System.out.println("	-Eso mi niño ¡Traela! ");
		}
		System.out.println("\n\n**" + getNombre() + " fue por la pelota corriendo súper rápido**");
		System.out.println("**Viene corriendo, va a  chocar contigo**");
		System.out.println("\nTu:	-¡Ay! ¡Muy bien! toma un premio ");
		
	}
	
	public void comer() {
		System.out.println("\n\nTu:	-Vamonos a casa, a comer");
		System.out.println("	-Cuando lleguemos te voy a dar tu comida favorita");
		System.out.println("\n**En casa "+ getNombre()+ " mueve su colita**");
		System.out.println("**Le das su comida en su platito**");
		System.out.println("**"+ getNombre() +" come feliz**");
		
	}
	
	public void pelear(String respuesta) {
		/**
		 * Recibe una respuesta
		 * si es SI, el perro se va a enfrentar a un perro bravo
		 * y va a poner a prueba las capacidades del amo
		 * si no, no
		 * */
		if(respuesta == "SI") {
			System.out.println("\n\nTu: 	-"+getNombre() + " ten cuidado, hay un perro bravo cerca");
			System.out.println("\n**"+ getNombre() +" gruñe**");
			System.out.println("\nGrrrrrrr");
			System.out.println("\n**Te alejas evitando correr para el que perro bravo no te siga**");
			System.out.println("**El perro se acercan y "+ getNombre()+" gruñe más fuerte**");
			System.out.println("\nGRRRRR GRRRRR");
			System.out.println("\n**"+ getNombre() +" tira de su correa**");
			if(getPeso() > 10) {
				/**
				 * Si el perro pesa más de 10 kg, o sea, es grande
				 * al tirar de la correa va a hacer que le amo la suelte.
				 * Si pesa menos, no va a lograr que el amo suelte la correa
				 * */
				System.out.println("** y se te suelta**\n");
				System.out.println("Tu: 	-"+ getNombre() + " ¡No! dejalo");
				System.out.println("\n**Corres**\n**Agarras un palo y alcanzas a agarrar la correa **");
				System.out.println("**Con el palo auyentas al otro perro**");
				System.out.println("**Te llevas lejos a " + getNombre()+"**");
				
				
			}else {System.out.println("**pero no se te suelta y te lo llevas lejos**");
			}
			System.out.println("\nTu:	-¿Cómo se te ocurre?\n	-Te pudo haber hecho daño ");
			System.out.println("	-Afortunadamente no te hizo nada \n\n**Lo abrazas**");
		
		}else {
			System.out.println("Tu:	-Hoy será un día tranquilo");
		}
	}
	
	public void muestraDatos (Perro perro) {
		System.out.println("La mascota que se te ha asignado es " + perro.getSexo() + 
							", tiene " + perro.getEdad() + " años y pesa " + perro.getPeso() + 
							" Kg. \nEs de raza tipo "+ perro.getRaza() + " y su pelaje es " + perro.getPelaje());
	}

}
