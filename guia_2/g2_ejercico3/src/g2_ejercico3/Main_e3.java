package g2_ejercico3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Main_e3 {

	public static void main(String[] args) throws IOException {
		
		BufferedReader reader = new BufferedReader( new InputStreamReader(System.in));
		
		System.out.println("Hola, escriba su nombre");
		String nombre = reader.readLine();
		

		
		
		Persona amo = new Persona(nombre);
		Perro perro = new Perro( 10, "Negro", "Mestizo", "Macho", 20);
		Parque parque = new Parque(amo, perro);
		parque.getPerro().muestraDatos(perro);
		System.out.println("Escriba el nombre de su nueva mascota");
		String nombre_perro = reader.readLine();
		parque.getPerro().setNombre(nombre_perro);
		System.out.println("Hola " + parque.getPersona().getNombre());
		System.out.println("Hola " + parque.getPerro().getNombre() + "\nUstedes están en el parque");
		
		boolean hembra = true;
		if(perro.getSexo() != "Hembra") {
			hembra = false;
			} 
		
		parque.getPerro().jugar(hembra);
		
		System.out.println("\n\n**Se escucha un gruñido**");
		System.out.println("¿Ves a un perro bravo?");
		String respuesta = reader.readLine();
		parque.grrrr(respuesta);
		
		parque.getPerro().comer();
		
		
		
		
		
	
		//System.out.println("\n" + parque.getPerro().getSexo());
		

	}

}
