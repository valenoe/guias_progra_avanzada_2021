package g2_ejercico3;


public class Parque {
	private Perro perro;
	private Persona persona;
	
	
	public Parque(Persona p_persona, Perro p_perro) {
		this.perro = p_perro;
		this.setPersona(p_persona);
	}

	public Perro getPerro() {
		return perro;
	}

	public void setPerro(Perro perro) {
		this.perro = perro;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	
	/*public void irParque(boolean hembra) {
		perro.jugar(hembra);
		
	}*/
	
	public void grrrr(String respuesta) {
		/**
		 * Convierte la respuesta recibida en letras mayusculas
		 *  */
		respuesta = respuesta.toUpperCase();
		//System.out.println(respuesta);
		
		/**
		 * Si la respuesta en mayusculas es S o SI
		 * la palabra que va a recibir perro.pelear() es un SI
		 * si es cualquier otra palabra, */
		String rfinal;
		switch(respuesta){
		case "S":
		case "SI": rfinal = "SI"; break;
		case "N":
		case "NO": rfinal = "NO"; break;
		default: rfinal = "NO";
		
			
		}
		perro.pelear(rfinal);
	}
	/*
	public void comer() {
		perro.comer();
	}*/
	
	
	
	

}


