package g2_ejercicio1;

public class Compra {
	private double valor;
	
	Compra (double precio){
		this.setValor(precio);
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

}
