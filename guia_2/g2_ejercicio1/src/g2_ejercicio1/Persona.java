package g2_ejercicio1;

import java.util.ArrayList;

public class Persona {
	private String nombre;
	private ArrayList<Tarjeta> billetera = new ArrayList<Tarjeta>();
	
	
	Persona(){}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
		
		System.out.println("El nombre de la titular es "+ this.nombre);
	}
	
	
	// Agrega tarjetas a la billetera
	public void agregarTarjeta(Tarjeta tarjeta) {
		billetera.add(tarjeta);
			
	}
	public ArrayList<Tarjeta> getBilletera() {
		return billetera;
	}
	

}
