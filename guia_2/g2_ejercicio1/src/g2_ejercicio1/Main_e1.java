package g2_ejercicio1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader ;
import java.util.ArrayList;

public class Main_e1 {

	public static void main(String[] args) throws IOException {
		
		Persona p;
		Tarjeta t1, t2, t3;
		ArrayList<Tarjeta> billetera = new ArrayList<Tarjeta>();
		
		
		// El atributo de persona se va aintroducir por terminal
		p = new Persona();
		
		System.out.print ( " Introduzca Nombre " );
		BufferedReader reader = new BufferedReader( new InputStreamReader(System.in));
		String nombre = reader.readLine ();
		
		p.setNombre(nombre);
		
		// Atributos de tarjeta
		// se crean las trajetas con su respectivo saldo
		t1 = new Tarjeta( 300);
		t2 = new Tarjeta(1000);
		t3 = new Tarjeta(2000);
		
		// las tarjetas se asocian a la persona
		t1.asocia_persona(p);
		t2.asocia_persona(p);
		t3.asocia_persona(p);
		
		// Y se meten las tarjetas a la billetera de la persona
		p.agregarTarjeta(t1);
		p.agregarTarjeta(t2);
		p.agregarTarjeta(t3);
		
		
		billetera = p.getBilletera();
		
		// Con esta función se imprime el contenido de la billetera
		printBilletera(billetera);
		
		
		System.out.println(nombre );
		Compra compra1 = new Compra(500);
		Compra compra2 = new Compra(1000);
		
		t1.cambio_saldo(compra1.getValor(), false);
		t2.cambio_saldo(compra2.getValor(), true);
		
		System.out.println(" ");
		printBilletera(billetera);
	}
		
	public static void printBilletera (ArrayList<Tarjeta> billetera ) {
		// Busca objetos de tipo tarjeta en la billetera (el ArrayList) y los imprime 
		for(Tarjeta t: billetera) {
			System.out.println(t.getTitular().getNombre() + " tiene en una tarjeta con "+ t.getSaldo());
		}
		
	}
	
		    
}


