package g2_ejercicio1;

public class Tarjeta {
	private double saldo;
	private Persona titular;
	
	/**tarjeta solo recibe saldo como parámetro
	 * Entonces, se asocia a una persona 
	 */
	Tarjeta(double saldo){
		this.saldo = saldo;
		
	}
	
	public void asocia_persona(Persona p) {
		this.setTitular(p);
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
		
	}
	
	public Persona getTitular() {
		return titular;
	}
	
	public void setTitular(Persona titular) {
		this.titular = titular;
	}
	
	
	public void cambio_saldo(double cambio, boolean sn ) {
		if (sn == true) {
			/**cambio es el valor de un objeto de clase Compra 
			 * entonces si la compra es real (true), va a disminuir el saldo
			*/
			this.setSaldo(this.getSaldo() - cambio); 
			System.out.println("Hizo una compra de "+ cambio);
		}else {
			// Si la compra no es real (false), o sea que es un abono, va a aumentar el saldo
			this.setSaldo(this.getSaldo() + cambio); 
			System.out.println("Hizo un abono de "+ cambio);
		}
	}
	
	
	

}
