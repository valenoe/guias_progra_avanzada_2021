package ejercicio2_guia6;

import java.util.HashSet;
import java.util.Iterator;

public class Controlador implements Interface{
	
	Persona p;
	HashSet<Integer> ruleta;
	
	public Controlador( HashSet<Integer> r, Persona p) {
		this.ruleta = r;
		// debe recibir a la persona completa para poder identificarlo
		this.p = p;
	}
	
	public void mostrar_carton() {
		// muestra los numeros de cada onjunto carton
		System.out.println("\nLa persona " + p.getIdentificador() + " tiene el siguiente carton");
		Iterator<Integer> iterate = p.getCarton().iterator();
		while(iterate.hasNext()) {
			System.out.println(iterate.next());
		}
	}
	public int comparar() {
		// su función es comprobar que todos los nuemros del carton estén
		// en los que ha tirado la ruleta 
		// si es así el jugador gana
		int gana = 0;
		if(ruleta.containsAll(p.getCarton())) {
			gana = 1;
			System.out.println("ha ganado la persona " + p.getIdentificador());
			System.out.println("con el carton: "+ p.getCarton());
		}
		
		return gana;
	}
	
	public void girar_la_ruleta() {
		// tira numeros aleatorios sin repeticion
		int cont = 0;
		int al = -1;
		if(ruleta.size() == 0) {
			al = (int)(Math.random()*99+1);
			ruleta.add(al);
		}else {
			while(cont == 0) {
				al = (int)(Math.random()*99+1);
				if(!ruleta.contains(al)) {
					ruleta.add(al);
					cont++;
			}
		}
			
		
	}
		System.out.println("\nEl numero que salió es :" + al);
		System.out.println("\nLa ruleta tiene los siguientes numeros");
		System.out.println(ruleta);
	}
	
	public void n_aleatorios_del_carton() {
		// tira exactamente 6 numeros aleatorios sin repeticion 
		for(int i =0; i<6;i++) {
			int cont = 0;
			if(p.getCarton().size() == 0) {
				int al = (int)(Math.random()*99+1);
				p.setCarton(al);
			}else {
				while(cont == 0) {
					int al = (int)(Math.random()*99+1);
					if(!p.getCarton().contains(al)) {
						p.setCarton(al);
						cont++;
					}
			}
		}
			
		
	}
		
	}
	
}
