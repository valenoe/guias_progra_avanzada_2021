package ejercicio2_guia6;

public class Main {

	public static void main(String[] args) {
		Persona p1, p2, p3;
		p1 = new Persona(1);
		p2 = new Persona(2);
		p3 = new Persona(3);
		
		// Persona que da los numeros
		Locutor l = new Locutor();
		
		// reparto de cartones
		Controlador c1, c2, c3;
		c1 = new Controlador(l.getRuleta(), p1);
		c1.n_aleatorios_del_carton();
		c1.mostrar_carton();
		c2 = new Controlador(l.getRuleta(), p2);
		c2.n_aleatorios_del_carton();
		c2.mostrar_carton();
		c3 = new Controlador(l.getRuleta(), p3);
		c3.n_aleatorios_del_carton();
		c3.mostrar_carton();
		
		// bingo
		int gana =0, gana1, gana2, gana3;
		while(gana == 0) {
			c1.girar_la_ruleta();
			gana1 = c1.comparar();
			gana2 = c2.comparar();
			gana3 = c3.comparar();
			if(gana1==1 || gana2 == 1 || gana3 == 1) {
				gana = 1;
			}

		}
		

	}
}
