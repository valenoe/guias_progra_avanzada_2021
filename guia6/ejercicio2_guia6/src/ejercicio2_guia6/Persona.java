package ejercicio2_guia6;
import java.util.HashSet;
//import java.util.Iterator;

public class Persona {
	private HashSet<Integer> carton;
	private int identificador;
	
	public Persona(int ID) {
		this.identificador = ID;
		carton = new HashSet<>();
	}

	public int getIdentificador() {
		return identificador;
	}

	public void setIdentificador(int identificador) {
		this.identificador = identificador;
	}

	public HashSet<Integer> getCarton() {
		return carton;
	}

	public void setCarton(int carton) {
		this.carton.add(carton);
	}
	
	
}
