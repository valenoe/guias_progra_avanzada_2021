package ejercio1_guia6;

import java.util.HashSet;
import java.util.Iterator;

public class Main {

	public static void main(String[] args) {
		
		HashSet<String> estudiantes = new HashSet<>();
		estudiantes.add("Daniel Tobar");
		estudiantes.add("Dante Aguirre");
		estudiantes.add("Jorje Carrillo");
		estudiantes.add("Cecilia Castillo");
		estudiantes.add("Michelle Valdes");
		estudiantes.add("Valery Chaparro");
		
		
		
		// mostrar los nombres
		System.out.println("Conjunto Estudiantes");
		Iterator<String> iterate = estudiantes.iterator();
		while(iterate.hasNext()) {
			System.out.println(iterate.next());
		}
		// Peguntar por un estdiante sne específico
		System.out.println("\n¿Existe la estudiante Cecilia Castillo en Programacion Avanzada? "+estudiantes.contains("Cecilia Castillo"));
		
		// Segundo conjunto de estudiantes
		HashSet<String> estudiantes2020 = new HashSet<>();	
		estudiantes2020.add("Benjamin Martin");
		estudiantes2020.add("Dinelly Pino");
		estudiantes2020.add("Martin Castillo");
		estudiantes2020.add("Nicolas Sepulveda");
		estudiantes2020.add("Paz Echeverria");
		
		System.out.println("\nConjunto Estudiantes2020");
		Iterator<String> iteratee = estudiantes2020.iterator();
		while(iteratee.hasNext()) {
			System.out.println(iteratee.next());
		}
		
		//Consulta si los elementos de conjunto estudiante existen en estudiantes2020
		System.out.println("\n¿Existe algun estudiante en el conjunto estudiante que tambien este en estudiantes2020? " 
		+ estudiantes2020.containsAll(estudiantes));
		
		//Union de los conjuntos
		estudiantes.addAll(estudiantes2020);
		System.out.println("\nConjunto Estudiantes unido a Estudiantes2020");
		System.out.println("Conjunto Estudiantes");
		System.out.println(estudiantes);
		
		// Borrar los elemtos de estudaiantes2020 que no esten en estudiantes
		estudiantes2020.retainAll(estudiantes);
		System.out.println(estudiantes2020);
		System.out.println("Ningun estudiantes se elimino ya que todos se encuantran en estudiantes");
		
		// Borrar los elemtos de estudaiantes que no esten en estudiantes2020
		System.out.println("\nPero al reves si se eliminan ya que no todos estan en estudiantes2020");
		estudiantes.retainAll(estudiantes2020);
		System.out.println(estudiantes);
		}

}
